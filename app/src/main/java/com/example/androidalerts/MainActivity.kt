package com.example.androidalerts

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.androidalerts.databinding.ActivityMainBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar

const val CHANNEL_ID_MAIN = "MAIN"
const val CHANNEL_ID_SECOND = "SECOND"


class MainActivity : AppCompatActivity() {
    var _binding: ActivityMainBinding? = null
    val binding: ActivityMainBinding get() = _binding!!
    private var notificationId = 0
    private val airplaneModeBroadcastReceiver = AirplaneModeBroadcastReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED).also {
            registerReceiver(airplaneModeBroadcastReceiver, it)
        }

        mainChannel()
        secondChannel()

        val view = ActivityMainBinding.inflate(layoutInflater).also { _binding = it }.root
        setContentView(view)

        binding.run {
            btnToast.toast("Toast")
            btnAlert.alert("Title", "Alert")
            btnNotification.notify("Title", "Notification")
            btnSnackBar.snackBar(view, "Yum")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(airplaneModeBroadcastReceiver)
    }

    private fun Button.toast(msg: String) {
        this.setOnClickListener() {
            Toast.makeText(
                this.context,
                msg,
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun Button.alert(title: String, msg: String) {
        this.setOnClickListener() {
            MaterialAlertDialogBuilder(this@MainActivity)
                .setTitle(title)
                .setMessage(msg)
                .setNeutralButton("Stop") { dialogInterface: DialogInterface, i: Int ->
                }
                .setPositiveButton("Yes") { dialogInterface: DialogInterface, i: Int ->
                }
                .show()
        }
    }

    private fun Button.snackBar(view: View, msg: String) {
        this.setOnClickListener() {
            Snackbar.make(
                view,
                msg,
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }

    private fun Button.notify(title: String, msg: String) {
        setOnClickListener {
            ++notificationId
            with(NotificationManagerCompat.from(this@MainActivity)) {
                notify(notificationId, getNotification(title))
            }
        }
    }

    private val isEven get() = notificationId % 2 == 0
    private val channelId get() = if (isEven) CHANNEL_ID_MAIN else CHANNEL_ID_SECOND
    private val pendingIntent: PendingIntent
        get() = Intent(
            this,
            if (isEven) MainActivity::class.java else SecondActivity::class.java
        ).apply { flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK }
            .run {
                PendingIntent.getActivity(
                    this@MainActivity,
                    0,
                    this,
                    PendingIntent.FLAG_IMMUTABLE
                )
            }
    private val priority =
        if (isEven) NotificationCompat.PRIORITY_HIGH else NotificationCompat.PRIORITY_DEFAULT

    private fun getNotification(title: String): Notification {
        return NotificationCompat.Builder(this@MainActivity, channelId)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(title)
            .setContentText("msg $notificationId")
            .setPriority(priority)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .build()
    }

    private fun createNotifcationChannel(
        channelName: String,
        channelDescription: String,
        id: String
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = channelName
            val descriptionText = channelDescription
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(id, name, importance).apply {
                description = descriptionText
            }
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun mainChannel() {
        createNotifcationChannel("Main", "Come back please.", CHANNEL_ID_MAIN)
    }

    private fun secondChannel() {
        createNotifcationChannel("Second", "Cheers mate.", CHANNEL_ID_SECOND)
    }
}