package com.example.androidalerts

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.androidalerts.databinding.SecondMainBinding

class SecondActivity : AppCompatActivity() {
    var _binding: SecondMainBinding? = null
    val binding: SecondMainBinding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val view = SecondMainBinding.inflate(layoutInflater).also { _binding = it }.root
        setContentView(view)
    }
}