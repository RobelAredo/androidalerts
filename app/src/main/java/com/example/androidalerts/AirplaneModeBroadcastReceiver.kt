package com.example.androidalerts

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast

class AirplaneModeBroadcastReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val isAirplaneModeEnabled = intent?.getBooleanExtra("state", false) ?: return
        if (isAirplaneModeEnabled) {
            Toast.makeText(context,"Airplane Mode is On", Toast.LENGTH_LONG).show()
        } else Toast.makeText(context,"Airplane Mode is Off", Toast.LENGTH_LONG).show()
    }
}